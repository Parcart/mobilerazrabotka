package com.example.myapplication.tools

import android.graphics.drawable.Drawable
import com.example.myapplication.R
import com.example.myapplication.Myapplication
import java.util.Calendar

data class astroSign(
    val startDay: Int,
    val startMonth: Int,
    val endDay: Int,
    val endMonth: Int,
    val img: Int
)
{

    companion object {
        private val signs = arrayOf(
            astroSign(21,3,20,4, R.drawable.kozerog),
            astroSign(21,4,21,5, R.drawable.telece),
            astroSign(22,5,21,6, R.drawable.blizneciu),
            astroSign(22,6,23,7, R.drawable.crab),
            astroSign(24,7,23,8, R.drawable.lev),
            astroSign(24,8,23,9, R.drawable.deva),
            astroSign(24,9,23,10, R.drawable.vesi),
            astroSign(24,10,22,11, R.drawable.scorpion),
            astroSign(23,11,21,12, R.drawable.strelec),
            astroSign(22,12,20,1, R.drawable.oven),
            astroSign(21,1,19,2, R.drawable.vodolei),
            astroSign(20,2,20,3, R.drawable.fishs),
        )
        fun getSignImageByDate(date: Calendar): Drawable?
        {
            val month = date.get(Calendar.MONTH)
            val day = date.get(Calendar.DAY_OF_MONTH)
            var result: Drawable? = null
            var resourceId: Int? = null
            signs.forEach {item ->
                if(item.startMonth == month)
                {
                    if(item.startDay <= day)
                    {
                        resourceId = item.img
                    }
                } else if (item.endMonth == month)
                {
                    if(item.endDay >= day)
                    {
                        resourceId = item.img
                    }
                }
            }
            resourceId?.let {resource->
                result = Myapplication.applicationContext().getDrawable(resource)
            }
            return result
        }
    }
}

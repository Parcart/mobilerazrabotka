package com.example.myapplication.database.Entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [
        ForeignKey(
            Person::class,
            arrayOf(
                "id"
            ),
            arrayOf(
                "personId"
            )
        )
    ]
)
data class PersonAvatar (
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val personId: Long,
    val image: ByteArray
){
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PersonAvatar

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}
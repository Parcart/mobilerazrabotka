package com.example.myapplication.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.myapplication.Myapplication
import com.example.myapplication.database.Entity.Departs
import com.example.myapplication.database.Entity.EmpDepart
import com.example.myapplication.database.Entity.Person
import com.example.myapplication.database.Entity.PersonAvatar
import com.example.myapplication.database.Entity.Teachers

class RoomDatabase {
    companion object
    {
        private var instance: Repository? = null
        fun getInstance(): Repository
        {
            if(instance == null){
                instance = Room.databaseBuilder(
                    Myapplication.applicationContext(),
                    Repository::class.java,
                    "persons"
                ).fallbackToDestructiveMigration().build()
            }
            return instance!!
        }
    }
}
@Database(
    entities =
    [
        Person::class,
        PersonAvatar::class,
        Departs::class,
        Teachers::class,
        EmpDepart::class
    ],
    version = 3
)
abstract class Repository: RoomDatabase()
{
    abstract fun personDAO(): PersonDAO
    abstract fun teachersDAO():TeachersDAO
}

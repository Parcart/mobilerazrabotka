package com.example.myapplication.database.Entity

import android.graphics.drawable.Drawable
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.Calendar

@Entity
data class Person (
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val lastName: String,
    val firstName: String,
    val surName: String,
    val dateBorn: String,
    val about: String,
)

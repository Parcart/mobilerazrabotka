package com.example.myapplication.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.myapplication.database.Entity.Departs
import com.example.myapplication.database.Entity.EmpDepart
import com.example.myapplication.database.Entity.Person
import com.example.myapplication.database.Entity.PersonAvatar
import com.example.myapplication.database.Entity.Teachers

@Dao
interface TeachersDAO {
    @Query("SELECT * FROM Departs")
    suspend fun getAllDeparts(): List<Departs>
    @Query("SELECT * FROM Departs WHERE Depart_ID=:id")
    suspend fun getDeparts(id: Int): List<Departs>
    @Query("SELECT * FROM Teachers")
    suspend fun getAllTeachers(): List<Teachers>
    @Query("SELECT * FROM EmpDepart")
    suspend fun getAllTEmpDepart(): List<EmpDepart>
    @Query("SELECT * FROM EmpDepart WHERE Depart_ID=:depart AND Emp_ID=:emp")
    suspend fun getEmpDepart(depart: Int, emp: Int): List<EmpDepart>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDeparts(item: Departs): Long
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTeachers(item: Teachers): Long
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEmpDepart(item: EmpDepart): Long
    @Query("SELECT * FROM Teachers WHERE Emp_ID=:id")
    suspend fun getTeachers(id: Int): Teachers


}
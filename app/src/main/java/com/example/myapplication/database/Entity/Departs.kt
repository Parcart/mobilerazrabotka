package com.example.myapplication.database.Entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Departs (
    @PrimaryKey
    val Depart_ID : Int,
    val DepartName : String,
    val DepartLN : String,
    val DepartSN : String
)
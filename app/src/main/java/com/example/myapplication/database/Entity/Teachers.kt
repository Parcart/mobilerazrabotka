package com.example.myapplication.database.Entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity
data class Teachers (
    @PrimaryKey
    val Emp_ID : Int,
    val FIO : String
)
package com.example.myapplication.database.Entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys =
    [
        ForeignKey(
            Departs::class,
            arrayOf(
                "Depart_ID"
            ),
            arrayOf(
                "Depart_ID"
            )
        ),
        ForeignKey(
            Teachers::class,
            arrayOf(
                "Emp_ID"
            ),
            arrayOf(
                "Emp_ID"
            )
        )
    ]
)
data class EmpDepart(
    @PrimaryKey(autoGenerate = true)
    val ID: Long,
    val Emp_ID : Int,
    val Depart_ID : Int
)

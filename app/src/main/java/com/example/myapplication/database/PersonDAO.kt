package com.example.myapplication.database

import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.toDrawable
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.example.myapplication.Myapplication
import com.example.myapplication.UI.PersonUI
import com.example.myapplication.database.Entity.Person
import com.example.myapplication.database.Entity.PersonAvatar
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

@Dao
    interface PersonDAO {
        @Query("SELECT * FROM Person")
        suspend fun getAllPerson(): List<Person>
        @Query("SELECT * FROM Person WHERE id=:id")
        suspend fun getPerson(id: Long): Person
        @Query("SELECT * FROM PersonAvatar WHERE personId=:id")
        suspend fun getAllPersonAvatars(id: Long): List<PersonAvatar>

        @Query("SELECT * FROM Person WHERE lastName=:lastName " +
                "AND firstName=:firstName " +
                "AND surName=:surName " +
                "AND dateBorn=:dateBorn " +
                "AND about=:about")
        suspend fun findPerson(
                       lastName: String,
                       firstName: String,
                       surName: String,
                       dateBorn: String,
                       about: String
        ): List<Person>
        suspend fun getPersonUI(): List<PersonUI>{
            val persons = getAllPerson()
            val presonsUI : MutableList<PersonUI> = mutableListOf()
            val context = Myapplication.applicationContext()
            val sdf = SimpleDateFormat("y-L-d", Locale.ENGLISH)
            persons.forEach{person: Person ->
                val personAvatar: List<PersonAvatar> = getAllPersonAvatars(person.id)
                var avatar : Drawable? = null
                if(personAvatar.isNotEmpty()){
                    val bitmap = BitmapFactory.decodeByteArray(
                        personAvatar[0].image,
                        0,
                        personAvatar[0].image.size)
                    if(bitmap != null){
                        avatar = bitmap.toDrawable(context.resources)
                    }
                }
                presonsUI.add(
                    PersonUI(
                        id = person.id,
                        userPhoto =  avatar,
                        lastName = person.lastName,
                        firstName = person.firstName,
                        surName = person.surName,
                        dateBorn = Calendar.getInstance().apply {
                        time = sdf.parse(person.dateBorn)!! },
                        about = person.about
                    )
                )
            }
            return presonsUI
        }
        suspend fun getPersonUI(id: Long): PersonUI{
            val person = getPerson(id)
            val context = Myapplication.applicationContext()
            val sdf = SimpleDateFormat("y-L-d", Locale.ENGLISH)
            val personAvatar: List<PersonAvatar> = getAllPersonAvatars(person.id)
            var avatar : Drawable? = null
            if(personAvatar.isNotEmpty()){
                val bitmap = BitmapFactory.decodeByteArray(
                    personAvatar[0].image,
                    0,
                    personAvatar[0].image.size)
                if(bitmap != null){
                    avatar = bitmap.toDrawable(context.resources)
                }
            }
            val presonsUI : PersonUI = PersonUI(
                id = person.id,
                userPhoto =  avatar,
                lastName = person.lastName,
                firstName = person.firstName,
                surName = person.surName,
                dateBorn = Calendar.getInstance().apply {
                    time = sdf.parse(person.dateBorn)!! },
                about = person.about)
            return presonsUI
        }
        @Insert(Person::class)
        suspend fun insertPerson(item: Person): Long
        @Insert(PersonAvatar::class)
        suspend fun insertAvatar(item: PersonAvatar): Long
    }

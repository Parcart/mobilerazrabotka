package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.Adapter.DepartsAdapter
import com.example.myapplication.Adapter.TeachersAdapter

import com.example.myapplication.databinding.FragmentListTeachersBinding


class ListTeachers : Fragment() {

    private lateinit var binding : FragmentListTeachersBinding
    private val args: ListTeachersArgs by navArgs()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            val nav = findNavController()
            nav.popBackStack()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListTeachersBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = TeachersAdapter(args.DepartId.toInt())
        adapter.setOnClickListener(object : TeachersAdapter.OnClickListener{
            override fun onClick(position: Int, model: com.example.myapplication.database.Entity.Teachers) {
                Log.d("Click", "Click")
                model.Emp_ID?.let {
                    val nav = findNavController()
                    nav.navigate(ListTeachersDirections.actionListTeachersToTeacherCard(model.Emp_ID.toLong()))
                }
            }
        })
        binding.recyclerView.layoutManager = LinearLayoutManager(this@ListTeachers.context)
        binding.recyclerView.adapter = adapter
    }

}
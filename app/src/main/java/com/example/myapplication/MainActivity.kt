package com.example.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.myapplication.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
//        val pfg = supportFragmentManager
        setContentView(binding.root)
        val navHost = supportFragmentManager.findFragmentById(R.id.fragmain)
        navHost?.let {
            val navController = navHost.findNavController()
            binding.bottomNavigationView.setupWithNavController(navController)
        }

//        val menu1cheack = binding.bottomNavigationView.menu.getItem(0).itemId
//        val menu2cheack = binding.bottomNavigationView.menu.getItem(1).itemId
//        binding.bottomNavigationView.setOnItemSelectedListener {item ->
//            when (item.itemId) {
//                menu1cheack-> {
//                    pfg.beginTransaction().apply {
//                        replace(com.example.myapplication.R.id.fragmain, BlankFragment())
//                        commit()}
//                    true
//                }
//                menu2cheack -> {
//                        pfg.beginTransaction().apply {
//                            replace(com.example.myapplication.R.id.fragmain, BlankFragment3_Main())
//                            commit()}
//                    true
//                }
//                else -> false
//            }
//
//        }
    }
}




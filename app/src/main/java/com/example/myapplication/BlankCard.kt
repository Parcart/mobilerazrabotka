package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.Adapter.UserAdapter
import com.example.myapplication.Data.PersonGetter
import com.example.myapplication.UI.PersonUI
import com.example.myapplication.database.RoomDatabase
import com.example.myapplication.databinding.FragmentBlankcardBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class   BlankCard : Fragment() {

    private lateinit var binding : FragmentBlankcardBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentBlankcardBinding.inflate(layoutInflater)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val coroutineScope = CoroutineScope(Dispatchers.Main)
        val dao = RoomDatabase.getInstance().personDAO()
        coroutineScope.launch {
            PersonGetter.storetoDatabase()
            val persons = withContext(Dispatchers.IO) {
                dao.getPersonUI()
            }
            val adapter = UserAdapter(
                persons
            )

            adapter.setOnClickListener(object : UserAdapter.OnClickListener{
                override fun onClick(position: Int, model: PersonUI) {
                    Log.d("Click", "Click")
                    model.id?.let {
                        val nav = findNavController()
                        nav.navigate(BlankCardDirections.actionToCard(model.id))
                    }
                }
            })
            binding.recyclerView.layoutManager = LinearLayoutManager(this@BlankCard.context)
            binding.recyclerView.adapter = adapter
        }

    }



}


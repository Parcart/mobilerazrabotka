package com.example.myapplication.api

import com.example.myapplication.api.Entity.Departs
import com.example.myapplication.api.Entity.Teachers
import com.example.myapplication.api.Entity.TeachersIniData
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitDAO {
    @GET("GetTeachersIniData")
    fun getTeachersIniData(): Call<TeachersIniData>
}
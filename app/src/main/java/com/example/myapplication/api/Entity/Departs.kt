package com.example.myapplication.api.Entity


data class Departs (
    val Depart_ID : Int,
    val DepartName : String,
    val DepartLN : String,
    val DepartSN : String
)
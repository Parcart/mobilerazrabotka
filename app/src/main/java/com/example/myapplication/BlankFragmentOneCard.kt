package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.myapplication.Data.PersonGetter
import com.example.myapplication.database.RoomDatabase
import com.example.myapplication.databinding.CardUserBinding
import com.example.myapplication.databinding.FragmentBlankOneCardBinding
import com.example.myapplication.tools.astroSign
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BlankFragmentOneCard : Fragment() {

    private val args: BlankFragmentOneCardArgs by navArgs()
    private lateinit var binding : FragmentBlankOneCardBinding
    private lateinit var mergeBinding : CardUserBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            val nav = findNavController()
            nav.popBackStack()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentBlankOneCardBinding.inflate(layoutInflater)
        mergeBinding = CardUserBinding.bind(binding.root)
        return mergeBinding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val coroutineScope = CoroutineScope(Dispatchers.Main)
        val dao = RoomDatabase.getInstance().personDAO()
        coroutineScope.launch {
            PersonGetter.storetoDatabase()
            val persons = withContext(Dispatchers.IO) {
                dao.getPersonUI(args.PersonId)
            }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
            binding.incldePersonCard.apply {
                with(persons){
                    firstNameID.text = firstName
                    surNameID.text = surName
                    lastNameID.text = lastName
                    userPhotoCard.setImageDrawable(userPhoto)
                    DateBorn.text = getDateOfBirthString()
                    znakImageCard.setImageDrawable(astroSign.getSignImageByDate(dateBorn))
                    aboutCard.text = about
                }
            }
        }

    }
}
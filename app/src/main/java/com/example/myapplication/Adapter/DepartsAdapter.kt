package com.example.myapplication.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.Data.DepartsTeachersGetter
import com.example.myapplication.R
import com.example.myapplication.UI.PersonUI
import com.example.myapplication.databinding.CardUserBinding
import com.example.myapplication.databinding.DepartsCardBinding
import kotlinx.coroutines.runBlocking

class DepartsAdapter : RecyclerView.Adapter<DepartsAdapter.ViewHolder>() {
    private var data: List<com.example.myapplication.database.Entity.Departs> = emptyList()
    init {
         runBlocking {
             DepartsTeachersGetter.getDeparts().collect { departs ->
                data = departs
            }
        }
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        private var binding: DepartsCardBinding
        init {
            binding = DepartsCardBinding.bind(view)
        }
        fun getBinding() = binding
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartsAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.departs_card ,parent,false
        )
        return DepartsAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: DepartsAdapter.ViewHolder, position: Int) {
        holder.getBinding().apply {
            with(data[position]){
                Depart.text = DepartName
                holder.itemView.setOnClickListener {
                    if (onClickListener != null) {
                        onClickListener!!.onClick(position, data[position])
                    }
                }
            }
        }
    }
    private var onClickListener: OnClickListener? = null
    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    // onClickListener Interface
    interface OnClickListener {
        fun onClick(position: Int, model: com.example.myapplication.database.Entity.Departs)
    }
}
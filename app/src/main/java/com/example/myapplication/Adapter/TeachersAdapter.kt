package com.example.myapplication.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.Data.DepartsTeachersGetter
import com.example.myapplication.R
import com.example.myapplication.databinding.TeachersCardBinding
import kotlinx.coroutines.runBlocking

class TeachersAdapter(private val id: Int) : RecyclerView.Adapter<TeachersAdapter.ViewHolder>() {
    private var data: List<com.example.myapplication.database.Entity.Teachers> = emptyList()
    init {
        runBlocking {
            DepartsTeachersGetter.getTeachers(id).collect { teachers ->
                data = teachers
            }
        }
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        private var binding: TeachersCardBinding
        init {
            binding = TeachersCardBinding.bind(view)
        }
        fun getBinding() = binding
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeachersAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.teachers_card,parent,false
        )
        return TeachersAdapter.ViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: TeachersAdapter.ViewHolder, position: Int) {
        holder.getBinding().apply {
            with(data[position]){
                TeacherName.text = FIO
                holder.itemView.setOnClickListener {
                    if (onClickListener != null) {
                        onClickListener!!.onClick(position, data[position])
                    }
                }
            }
        }
    }
    private var onClickListener: OnClickListener? = null
    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    // onClickListener Interface
    interface OnClickListener {
        fun onClick(position: Int, model: com.example.myapplication.database.Entity.Teachers)
    }
}
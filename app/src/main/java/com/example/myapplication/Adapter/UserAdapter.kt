package com.example.myapplication.Adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.UI.PersonUI
import com.example.myapplication.R
import com.example.myapplication.databinding.CardUserBinding
import com.example.myapplication.tools.astroSign

class UserAdapter (private val data: List<PersonUI>)
    :RecyclerView.Adapter<UserAdapter.ViewHolder>()
{
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    {
        private var binding: CardUserBinding
        init {
            binding = CardUserBinding.bind(view)
        }
        fun getBinding() = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.card_user,parent,false
        )
        return ViewHolder(view)
    }

    override fun getItemCount() = data.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.getBinding().apply {
            with(data[position]){
                firstNameID.text = firstName
                surNameID.text = surName
                lastNameID.text = lastName
                userPhotoCard.setImageDrawable(userPhoto)
                DateBorn.text = getDateOfBirthString()
                znakImageCard.setImageDrawable(astroSign.getSignImageByDate(dateBorn))
                aboutCard.text = about.substring(  0,25 )+"..."
                holder.itemView.setOnClickListener {
                    if (onClickListener != null) {
                        onClickListener!!.onClick(position, data[position])
                    }
                }

            }
        }
    }
    private var onClickListener: OnClickListener? = null
    fun setOnClickListener(onClickListener: OnClickListener) {
        this.onClickListener = onClickListener
    }

    // onClickListener Interface
    interface OnClickListener {
        fun onClick(position: Int, model: PersonUI)
    }
}

package com.example.myapplication

import android.graphics.Path.Direction
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.myapplication.Data.DepartsTeachersGetter
import com.example.myapplication.api.Entity.Departs
import com.example.myapplication.api.Entity.TeachersIniData
import com.example.myapplication.api.RetrofitDAO
import com.example.myapplication.database.Entity.EmpDepart
import com.example.myapplication.database.Entity.Teachers
import com.example.myapplication.database.RoomDatabase
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.databinding.FragmentBlankBinding
import com.example.myapplication.databinding.FragmentBlankFragment3MainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BlankFragment : Fragment() {
    private lateinit var binding : FragmentBlankBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBlankBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.button1fg1.setOnClickListener{
            val nav = findNavController()
            nav.navigate(BlankFragmentDirections.actionBlankFragmentToBlankFragment2())
        }
        binding.button3.setOnClickListener {
            val dao = RoomDatabase.getInstance().teachersDAO()
            val coroutineScope = CoroutineScope(Dispatchers.Main)
            coroutineScope.launch {
                val Teacherall = withContext(Dispatchers.IO){
                    dao.getAllTeachers()
                }
                val Teacher = withContext(Dispatchers.IO){
                    dao.getTeachers(47)
                }
                val ttt = withContext(Dispatchers.IO){
                    dao.getAllDeparts()
                }
                if (Teacher.FIO == "123"){}
                if (Teacherall.isEmpty()){}
                if (ttt.isEmpty()){}
            }

        }

    }

}
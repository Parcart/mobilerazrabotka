package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.databinding.FragmentBlankFragment3MainBinding


class BlankFragment3_Main : Fragment() {
    private lateinit var binding : FragmentBlankFragment3MainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBlankFragment3MainBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.button.setOnClickListener {
            val share = Intent()
            share.action = Intent.ACTION_SEND
            share.type = "text/plan"
            share.putExtra(Intent.EXTRA_TEXT, binding.textView.text)
            startActivity(Intent.createChooser(share,"pattern"))
        }
    }


}
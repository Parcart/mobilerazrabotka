package com.example.myapplication.UI
import android.graphics.drawable.Drawable
import java.util.Calendar

data class PersonUI(
    val id: Long?,
    val userPhoto: Drawable?,
    val lastName: String,
    val firstName: String,
    val surName: String,
    val dateBorn: Calendar,
    val about: String,

    ){
    fun getDateOfBirthString():String {
        return this.dateBorn[Calendar.DAY_OF_MONTH].toString() + "." +
                this.dateBorn[Calendar.MONTH].toString() + "." +
                this.dateBorn[Calendar.YEAR].toString()
    }
}

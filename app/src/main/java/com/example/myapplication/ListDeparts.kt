package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.Adapter.DepartsAdapter
import com.example.myapplication.Adapter.UserAdapter
import com.example.myapplication.Data.DepartsTeachersGetter
import com.example.myapplication.UI.PersonUI
import com.example.myapplication.database.RoomDatabase
import com.example.myapplication.databinding.DepartsCardBinding
import com.example.myapplication.databinding.FragmentBlank2Binding
import com.example.myapplication.databinding.FragmentListDepartsBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ListDeparts : Fragment() {

    private lateinit var binding : FragmentListDepartsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            val nav = findNavController()
            nav.popBackStack()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListDepartsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val coroutineScope = CoroutineScope(Dispatchers.Main)
        coroutineScope.launch {
            DepartsTeachersGetter.updateDatabase()
            val adapter = DepartsAdapter()
            adapter.setOnClickListener(object : DepartsAdapter.OnClickListener{
                override fun onClick(position: Int, model: com.example.myapplication.database.Entity.Departs) {
                    Log.d("Click", "Click")
                    model.Depart_ID?.let {
                        val nav = findNavController()
                        nav.navigate(ListDepartsDirections.actionListDepartsToListTeachers(model.Depart_ID.toLong()))
                    }
                }
            })
            binding.recyclerView.layoutManager = LinearLayoutManager(this@ListDeparts.context)
            binding.recyclerView.adapter = adapter
        }

    }
}
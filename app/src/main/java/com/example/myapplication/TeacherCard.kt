package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavArgs
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.myapplication.Data.DepartsTeachersGetter
import com.example.myapplication.database.Entity.Teachers
import com.example.myapplication.databinding.FragmentBlankOneCardBinding
import com.example.myapplication.databinding.TeachersCardBinding
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class TeacherCard : Fragment() {

    private val args: TeacherCardArgs by navArgs()
    private lateinit var binding : TeachersCardBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val callback = requireActivity().onBackPressedDispatcher.addCallback(this) {
            val nav = findNavController()
            nav.popBackStack()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = TeachersCardBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val teacherFlow : Flow<Teachers> = DepartsTeachersGetter.getTeacher(args.TeacherID.toInt())
        viewLifecycleOwner.lifecycleScope.launch {
            teacherFlow.collect { teacher ->
                binding.TeacherName.text = teacher.FIO
            }
        }
    }

}
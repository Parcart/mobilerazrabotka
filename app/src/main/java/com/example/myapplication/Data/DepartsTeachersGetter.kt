package com.example.myapplication.Data


import com.example.myapplication.api.Entity.Departs
import com.example.myapplication.api.Entity.TeachersIniData
import com.example.myapplication.api.RetrofitDAO
import com.example.myapplication.database.Entity.EmpDepart
import com.example.myapplication.database.Entity.Teachers
import com.example.myapplication.database.RoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DepartsTeachersGetter {
    companion object
    {
        private val dao = RoomDatabase.getInstance().teachersDAO()
        private val coroutineScope = CoroutineScope(Dispatchers.Main)
        fun getDeparts(): Flow<List<com.example.myapplication.database.Entity.Departs>> {
            return flow {
                    val ListDeparts = withContext(Dispatchers.IO){
                        dao.getAllDeparts()
                    }
                    emit(ListDeparts)
            }
        }
        fun getTeachers(depart:Int): Flow<List<Teachers>> {
            return flow {
                val ListAllTEmpDepart = withContext(Dispatchers.IO){
                    dao.getAllTEmpDepart()
                }
                val ListTeachers: MutableList<Teachers> = mutableListOf()
                ListAllTEmpDepart.forEach { unit ->
                    if(unit.Depart_ID == depart){
                        val Teacher = withContext(Dispatchers.IO){
                            dao.getTeachers(unit.Emp_ID)
                        }
                        ListTeachers.add(Teacher)
                    }
                }
                emit(ListTeachers)
            }
        }
        fun getTeacher(id: Int): Flow<com.example.myapplication.database.Entity.Teachers> {
            return flow {
                val Teacher = withContext(Dispatchers.IO){
                    dao.getTeachers(id)
                }
                emit(Teacher)
            }
        }

        fun updateDatabase(){
            val retrofit = RetrofitClient.getInstance("https://api-1.ursei.su/schedule/")
            val retrofitDAO = retrofit.create(RetrofitDAO::class.java)
            retrofitDAO.getTeachersIniData().enqueue(object : Callback<TeachersIniData> {
                override fun onFailure(call: Call<TeachersIniData>, t: Throwable) {
                    // Обработка ошибки. Ошибоки быть не может XD
                }

                override fun onResponse(call: Call<TeachersIniData>, response: Response<TeachersIniData>) {
                    coroutineScope.launch {
                        response.body()?.Departs?.forEach { departs: Departs ->
                            val createDepart = withContext(Dispatchers.IO) {
                                dao.insertDeparts(com.example.myapplication.database.Entity.Departs(departs.Depart_ID,departs.DepartName,departs.DepartLN,departs.DepartSN))
                            }
                        }
                        response.body()?.Teachers?.forEach { teachers ->
                            val createTeachers = withContext(Dispatchers.IO) {
                                dao.insertTeachers(Teachers(teachers.Emp_ID, teachers.FIO))
                            }
                            val getEmpDepart = withContext(Dispatchers.IO) {
                                dao.getEmpDepart(teachers.Depart_ID, teachers.Emp_ID)
                            }
                            if (getEmpDepart.isEmpty()){
                                val createEmpDepart = withContext(Dispatchers.IO) {
                                    dao.insertEmpDepart(EmpDepart(0,teachers.Emp_ID,teachers.Depart_ID))
                                }
                            }
                        }
                    }

                }
            })
        }
    }
}